---
title: "TP2 Statistical Analysis and Document Mining"
author: "Karim Mike Rahhal"
date: "01/03/2022"
output:
  html_document:
    df_print: paged
    fig_caption: yes
  pdf_document: default
---

```{r}
library("maps")
library('fields')
library("FactoMineR")
library("factoextra")
```
## 1 Data

First, we remove the lines of the dataset which corresponds to the same population because we only want to plot the populations available in the data. Then, we define the coordinate vector (latitude and longitude) of each unique populuation. So, we use the maps library to show the world map and inside it we plot the population coordinates and add their names in the legend part of the code.
```{r}
NAm2 = read.table("NAm2.txt",header=TRUE) 
names = unique(NAm2$Pop)
# returns a vector, data frame or array like x but with duplicate elements/rows removed.
npop = length(names)

coord = unique(NAm2[,c("Pop","long","lat")])
# coordinates for each pop

colPalette =rep (c(" black ","red "," cyan "," orange "," brown "," blue "," pink "," purple "," darkgreen ") ,3)
# colors used in the map

pch = rep(c(16 ,15 ,25) , each =9)
#points shape in the plot


plot ( coord [,c("long","lat")] , pch = pch , col = colPalette , asp =1)
# asp allows to have the correct ratio between axis longitude
# and latitude , thus the map is not deformed

legend ("bottomleft",legend =names , col = colPalette, lty = -1, pch = pch, cex =0.75, ncol =2, lwd =2)
map ("world",add =T )
```


## 2 Regression

By using all genetic markers as predictors, we're going to perform a multiple linear regression model to predict the longitude of each individual. First, we define the model of our regression :

```{r}
#First we created a data.frame NAaux containing the regular Data without columns 1 till 7
NAaux = NAm2[,-c(1:7)]
#define the model
model<-lm(NAm2$long ~., data=NAaux)
head(summary(model)$coefficients)
```
We remark that we have NaN values in the error, t and p values and also some of the predicted coefficients. This is caused by the fact that the covariance matrix in our case is not invertible.

In fact, the number of predictors is greater than the number of data. If we take X as an $(n,p)$ matrix whith p>n. We know that $rank(X) <= min(n,p)=n$ so there exists a vector v such that $Xv = 0$ so $X^{T}Xv = 0$ which means that $X^{T}X$ is not invertible.

PCA -> reduce dimension of predictors will be used 


## 3 PCA
## (a)
\
PCA consists in summarizing a large number $p$ of variables by a smaller number $d$ of variables which are called principal components, keeping the main part of the information. Moreover, PCA determines graphical representations in the plane, called "maps", that allows us to better understand proximity between individuals.

## (b)
Here, we perform PCA on the genetic data of all the individials :
```{r}
pcaNAm2<-prcomp(NAm2[,-c(1:8)], center =TRUE,scale=FALSE)
pcaNAm<-prcomp(NAm2[,-c(1:8)], center=TRUE,scale=TRUE) 
```
We should not use the argument scale because the data does not have multiple variables with different size-range, but most of the data is only a sequence of 0 and 1.

## (c)
We perform PCA on the data using scaling and without it and we will analyze the differences .
```{r}
caxes =c(1 ,2)
plot(pcaNAm2$x[, caxes],col =" white ",main="Figure 2-populations on the first two principal axes with Scale=F")
for ( i in 1: npop )
{
# print(names[i])
lines(pcaNAm2$x[which(NAm2[ ,3]==names[i]),caxes], type ="p",col= colPalette[i], pch = pch[i])
# legend ("top",legend =names,col = colPalette,lty = -1 ,pch = pch ,cex =0.75 ,ncol =3 ,lwd =2)
}
```
```{r}
caxes =c(1 ,2)
plot(pcaNAm$x[, caxes],col =" white",main="Figure 3-populations on the first two principal axes with Scale=T")
for ( i in 1: npop )
{
# print(names[i])
lines(pcaNAm$x[which(NAm2[ ,3]==names[i]),caxes], type ="p",col= colPalette[i], pch = pch[i])
# legend ("top",legend =names,col = colPalette,lty = -1 ,pch = pch ,cex =0.75 ,ncol =3 ,lwd =2)
}
```
\
The first thing we can remark is that without using scaling we notice that the groups of close populations are well defined and with scaling the population projections are more scattered around the map because we're losing some variance when we scale the data.

As we can see in Figure 2 only the populations Ache and Surui are easily identified since these populations have a well structured 'heap' of points in the graph, whereas the others are either on top of each other or have no clear placement in the graph. It can be argued that also Karitiana is fairly easy to identify.
\

```{r}
# using the principle components 5 and 6 
caxes =c(5 ,6)
plot(pcaNAm2$x[, caxes ],col =" white ",main="Figure 4-populations on the 5th and 6th principal axes of PCA")
for ( i in 1: npop )
{
# print(names[i])
lines(pcaNAm2$x[which(NAm2[ ,3]==names[i]),caxes], type ="p",col= colPalette[i], pch = pch[i])
# legend ("top",legend =names,col = colPalette,lty = -1 ,pch = pch ,cex =0.75 ,ncol =3 ,lwd =2)
}
```
\
Observing the graph using only the $5$th and $6$th principal axes, it is clear that Ache again is easily identified. In addition, the populations Karitiana, Prima and Chipewyan are easy to identify.

## (d)

```{r}
# Histogram of the explained variance
fviz_eig(pcaNAm2, addlabels = TRUE, ylim = c(0, 3), main = "scale = F")
# Variances related to the components
lambda = pcaNAm2$sdev^2
# Cumulative variances of the 494 components
cumulative_variance = cumsum(lambda/sum(lambda)) 
inertia_2_first_components = cumulative_variance[2]
cat(" The percentage of variance captured by the first two principal components is:",inertia_2_first_components,"\n")
```
Only $3.6$% of variance is captured by the first 2 principal components based on the Histogram above.
\
```{r}
# Cumulative Proportion is the cumulative sum of the proportion of variance
plot(main = "Explained variance vs number of predictors",cumsum(pcaNAm2$sdev^2 / sum(pcaNAm2$sdev^2)), type="b",ylab = "Explained variance", 
      xlab = "number of predictors")
# determine the number of predictors for which the explained variance is higher than 80%
plot(main = "number of predictors with explained variance higher than 80%",cumsum(pcaNAm2$sdev^2 / sum(pcaNAm2$sdev^2))>= 0.8, type="b",ylab = "Explained variance", 
      xlab = "number of predictors")
```
\
We want a compromise between the number of predictors(as low as possible) and the explained variance of the original model.
Starting from the $265$th principal components we have a very decent description of our data, as about $80$% of variance is captured by the first $265$ principal components.

## 4 PCR

## (a)
Predict the values of the longitude and latitude using the PCA scores
```{r}
pca <- function(pca_nbr){
  #Creating a data frame
  y1 <- NAm2$long
  y2 <- NAm2$lat
   
  #fiting the linear model
  model_pca_long<-lm(y1~.,data = as.data.frame(pcaNAm2$x[,1:pca_nbr]))
  
  model_pca_lat <-lm(y2~.,data = as.data.frame(pcaNAm2$x[,1:pca_nbr]))
  
  #predicts the future values
  lmlat  <- predict(model_pca_lat)
  lmlong <- predict(model_pca_long)
  
  plot ( fitted(model_pca_long),#lmlong$fitted.values ,
        fitted(model_pca_lat),#lmlat$fitted.values ,
        col = "white",
        asp = 1)
  for (i in 1:npop)
  {
    # print (names [i])
    lines (
      fitted(model_pca_long)[which (NAm2 [, 3] == names [i])] ,
      fitted(model_pca_lat)[which (NAm2 [, 3] == names [i])] ,
      type = "p",
      col = colPalette [i],
      pch = pch [i]
    )
  }
  # 
  # legend (
  #   "bottomleft",
  #   legend = names ,
  #   col = colPalette ,
  #   lty = -1 ,
  #   pch = pch ,
  #   cex = .75 ,
  #   ncol = 3 ,
  #   lwd = 2
  # )
  map ("world", add = T)
}
pca(494)
pca(250) #compare this to the original map
```
\
PCA with 250 principle components Compared to the graph in exercise 1, it seems quite reasonable, not too optimistic, nor too pessimistic considering we can detect and distinguish between the same populations in both of the graphs. But, As we can see when the number of Principle Component Axis increases we are getting closer to the original Map because we're retaining all the variance of the original model by using most of the PCA axes.

## (b)
Now we evaluate the error of the regression model by calculation the orthodromic distance  between the predicted latitude and longitude using as predictors the first 250 principal axes
```{r}
library("fields")
library("viridis")

y1 <- NAm2$long
y2 <- NAm2$lat

#fiting the linear model
model_pca_long <-lm(y1 ~ ., data = as.data.frame(pcaNAm2$x[, 1:250]))
model_pca_lat <- lm(y2 ~ ., data = as.data.frame(pcaNAm2$x[, 1:250]))

#predicts the future values
lmlong  <- predict(model_pca_long)
lmlat <- predict(model_pca_lat)


x1 <- cbind(lmlong, lmlat)
x2 <- cbind(NAm2$long, NAm2$lat)

a <- sum(diag(rdist.earth(x1, x2, miles=F)))/494

cat("the the mean error of the previous model built using (the first) 250 principal axes is:", a)
```

## 5 PCR and cross-validation

## a
In the cross-validation, we divide the dataset into a training set and a validation set usually with a 80 -20% ratio then we use our training set to predict the data in our validation set. We get two errors, one is called the traininig error and the other is the validation error and we plot both of them in a graph to choose the best number of predictors.
```{r}
# create set of indexes of data that gives the index of the set for which it belongs
labels =rep (1:10 , each =50)
set= sample (labels ,494)
```


## b-1 
We create an empty dataframe predictedCoord with 2 columns ("longitude", "latitude") and as many rows as there are individuals.
```{r}
naxes = 4
predictCoord <- as.data.frame(matrix(nrow = 494,ncol = 2))
colnames(predictCoord) <- c("longitude","latitude")
```

## b-2
Using as predictors the scores of the first 4 PCA axes, we will explain latitude and longitude
using the individuals who do not belong to the validation set number 1..
```{r}
# Creating the training set by removing the first validation set
indexes <- set != 1
training <- NAm2[indexes,]
print(training)
#Creating a data frame without including the first validation set
y1 <- training$long
y2 <- training$lat
Gen = NAm2[,-c(1:8)]
pcaNAm2 <- prcomp(Gen, scale = F)

#fiting the linear model
model_pca_long<-lm(y1~.,data = as.data.frame(pcaNAm2$x[indexes,1:4]))
model_pca_lat <-lm(y2~.,data = as.data.frame(pcaNAm2$x[indexes,1:4]))
```


## b-3 
Using the estimated model, we will predict latitude and longitude for individuals belonging to the validation set number 1. We will store the predicted coordinates into predictCoord matrix.
```{r}
# define the first validation set
indexes <- set == 1
valid <- NAm2[indexes,1:4]

#predicts the future values
lmlat  <- predict(model_pca_long, newdata = as.data.frame(pcaNAm2$x[indexes,1:4]))
lmlong <- predict(model_pca_lat, newdata = as.data.frame(pcaNAm2$x[indexes,1:4]))

# store them in predicted coordinates matrix
predictCoord[indexes,1] = lmlat
predictCoord[indexes,2] = lmlong
```

## b-4
Here we fill the prediction matrix with all predictions done using all the validation sets.
```{r}

for (i in 2:10) {
  # Creating the training set by removing the first validation set
  indexes <- set != i
  training <- NAm2[indexes,]
  
  #Creating a data frame without including the first validation set
  y1 <- training$long
  y2 <- training$lat
   
  #fiting the linear model
  model_pca_long<-lm(y1~.,data = as.data.frame(pcaNAm2$x[indexes,1:4]))
  model_pca_lat <-lm(y2~.,data = as.data.frame(pcaNAm2$x[indexes,1:4]))
  
 # define the first validation set
  indexes <- set == i
  
  #predicts the future values
  lmlong  <- predict(model_pca_long, newdata = as.data.frame(pcaNAm2$x[indexes,1:4]))
  lmlat <- predict(model_pca_lat, newdata = as.data.frame(pcaNAm2$x[indexes,1:4]))
  
  # store them in predicted coordinates matrix
  predictCoord[indexes,1] = lmlong
  predictCoord[indexes,2] = lmlat
}

```

the error is defined by :
```{r}
mean(rdist.earth.vec(x2, predictCoord, miles = FALSE))
```
## c
We perform the same analysis with principal component axes number going from 2 to 440 by a step of 10
```{r}
naxe_tot = 440
valid_error = rep(0,(naxe_tot-2)/10 + 1) #repeating zero 44 times bacause we have approx 44 cases 
training_error = rep(0,(naxe_tot-2)/10 + 1)
j = 1
for (naxes in seq(2 , naxe_tot, by = 10)) { # loop through the number of principal axes (2,12,22,32,...432)
  
  predictCoord <- as.data.frame(matrix(nrow = 494,ncol = 2))
  colnames(predictCoord) <- c("longitude","latitude")
  
  for (i in 1:10) { # loop through the validation sets
    
    # Creating the training set by removing the i th validation set
    indexes <- set != i
    training <- NAm2[indexes,]
    
    #Creating a data frame without including the i th validation set
    y1 <- training$long
    y2 <- training$lat
     
    #fitting the linear model
    model_pca_long<-lm(y1~.,data = as.data.frame(pcaNAm2$x[indexes,1:naxes]))
    model_pca_lat <-lm(y2~.,data = as.data.frame(pcaNAm2$x[indexes,1:naxes]))
    
    # define the i th validation set
    indexes <- set == i
    
    #predicts the future values
    lmlong  <- predict.lm(model_pca_long, newdata = as.data.frame(pcaNAm2$x[indexes,1:naxes]))
    lmlat <- predict.lm(model_pca_lat, newdata = as.data.frame(pcaNAm2$x[indexes,1:naxes]))
    
    # store them in predicted coordinates matrix
    predictCoord[indexes,1] = lmlong
    predictCoord[indexes,2] = lmlat
      
    # Update the training error
    x3 = cbind(y1,y2)
    x4 = cbind(model_pca_long$fitted.values,model_pca_lat$fitted.values)
    training_error[j] = training_error[j] + mean(rdist.earth.vec(x3, x4, miles = FALSE))
  
  }
  training_error[j] =  training_error[j]/10
  valid_error[j] = mean(rdist.earth.vec(x2, predictCoord, miles = FALSE))
  j = j + 1
}

principal_comp = seq(2 , naxe_tot, by = 10)
plot(main = "Error vs number of principal components",principal_comp, valid_error, type = "l", lwd = 2, col = gray(0.4), ylab = "error", 
      xlab = "number of pca axes", log = "x",ylim=range(c(valid_error,training_error)))
lines(principal_comp, training_error, lwd =5, col = gray(0.4))
lines(principal_comp, valid_error, lwd = 2, col = "darkred")
lines(principal_comp, valid_error + training_error, lwd = 2, col = "green")
legend(x = "top", legend = c("Training error", "Prediction error","Total error" ), lwd = rep(2, 2), 
col = c(gray(0.4), "darkred","green"), text.width = 0.6, cex = 1)
```


## d
We would keep the model with the minimum sum of prediction and training error, from the graph of the previous question we can see that it corresponds to choosing approximately 150 principal component axes.

```{r}
training_error = 0
predictCoord <- as.data.frame(matrix(nrow = 494,ncol = 2))
colnames(predictCoord) <- c("longitude","latitude")
for (i in 1:10) {
  # Creating the training set by removing the first validation set
  indexes <- set != i
  training <- NAm2[indexes,]
  
  #Creating a data frame without including the first validation set
  y1 <- training$long
  y2 <- training$lat
   
  #fiting the linear model
  model_pca_long<-lm(y1~.,data = as.data.frame(pcaNAm2$x[indexes,1:150]))
  model_pca_lat <-lm(y2~.,data = as.data.frame(pcaNAm2$x[indexes,1:150]))
  
 # define the first validation set
  indexes <- set == i
  
  #predicts the future values
  lmlong  <- predict(model_pca_long, newdata = as.data.frame(pcaNAm2$x[indexes,1:150]))
  lmlat <- predict(model_pca_lat, newdata = as.data.frame(pcaNAm2$x[indexes,1:150]))
  
  # store them in predicted coordinates matrix
  predictCoord[indexes,1] = lmlong
  predictCoord[indexes,2] = lmlat
  
  # Update the training error
  x3 = cbind(y1,y2)
  x4 = cbind(model_pca_long$fitted.values,model_pca_lat$fitted.values)
  training_error = training_error + mean(rdist.earth.vec(x3, x4, miles = FALSE))
}
training_error =  training_error/10
cat("The training error for 150 PCA axes is :","\n")
training_error
cat("The prediction error for 150 PCA axes is :","\n")
mean(rdist.earth.vec(x2, predictCoord, miles = FALSE))
# Plot the predicted coordinates
pca(150)
```
\
We remark that the training error is lower than our prediction error in this model.

## Conclusion :

Our goal in the labwork is to build the best predictive model to predict individual geographical coordinates.However,there are too many predictors (more than 5000 degrees of freedom), for too few observations ($n=494$), this doesn’t make the Linear Regression work,then we perform PCA to reduce dimension of predictors,then we have to choose the best number of pca axes which minimizes the prediction error
At first glance, we will think that choosing more PCA axes as predictors will give us more information on the model so it will be better, but after analyzing the prediction error we remarked that choosing 150 PCA axes was a better choice because of how big the prediction error becomes. So 400 axes for example doesn't perform as well as 150 on different datasets of genetic code. The only problem we can see is that the dataset we have is too small (494 data points), so the model could perhaps not be useful for datasets with a large number of samples.


